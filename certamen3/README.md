# Certamen 3 TEL 335 #
Gabriel Fuentes Vergara

Descripcion de la entrega:

1 Main Panel (Layout)
    -> Se encarga de solicitar y almacenar el token para todos los componentes
    -> Maneja el estado 'loaded' para revisar cuando cargue la página
    -> Renderiza una Lista de productos, cuando el boton se presiona
    -> 

1 Componentes:
    Spinner -> Gestiona el spinner de carga
    Lista -> Despliega la lista de productos (si el campo de busqueda esta vacio)
    GeneradorCards -> Dada una lista de productos, genera las Cards para mostrarlos por pantalla
    Search ->  Cuando el usuario ingresa mas de 3 caracteres en el campo de busqueda, se realiza el POST a la API y mostramos los resultados
