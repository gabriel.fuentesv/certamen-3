import { Form, Button, Container } from 'react-bootstrap'
import { useState } from 'react'
import axios from 'axios'

function Search (props) {
    const [busqueda, setBusqueda] = useState('')
    async function actualizarBusqueda(event){
        setBusqueda(event.target.value)
        if(event.target.value.length > 3){
            //console.log("Buscando: ")
            //console.log(event.target.value.length)
            setBusqueda(event.target.value.length)
            let url = 'http://api.telematica.xkivertech.cl:4000/products/' + event.target.value.length;
            let options = {
                method: 'POST',
                headers: {
                    'content-type': 'application/x-www-form-urlencoded',
                    'access-token': props.token
                },
                url
            }
            const respAuth = await axios(options);
            if(respAuth.data ){
                //TO DO: Desplegar lista de resultados como CARDS

            }
        }
    }
    return (
        <div>
            <Container>
                <Form>
                    <Form.Group controlId="formBusqueda">
                        <Form.Label>Que producto buscas ?</Form.Label>
                        <Form.Control type="text" placeholder="Ingresa busqueda" onChange={actualizarBusqueda} />
                        <Form.Text className="text-muted">
                        Puede indicar sólo una parte del nombre, y trataremos de adivinar el producto !
                        </Form.Text>
                    </Form.Group>
                </Form>
            </Container>
        </div>
    )
}

export default Search