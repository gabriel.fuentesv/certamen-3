
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

function GeneradorCards (props){
    async function generarCard (prod,i){
        return(
            <div className = "container">
                <Card style={{ width: '18rem' }} key= {prod.id + i} className ='box'>

                    <Card.Img variant="top" src={'http://'+ prod.image} />
                    <Card.Body>
                        <Card.Title>{prod.brand}</Card.Title>
                        <Card.Text>
                            <br />
                            {prod.description}
                            <br />
                            <br />
                            {prod.price}
                            <br />
                        </Card.Text>
                        <Button variant="primary">Saquear</Button>
                    </Card.Body>
                </Card>
            </div>
        )
    }
        return (
            <section id="productList">
                <div className="container" style={{marginTop:50}} >
                    <div className="row">
                        {   
                            props.productos.map((i,prod) =>{
                                return(
                                    <div className="container">
                                    {generarCard(prod,i)}
                                    </div>
                                )
                            })
                        }
                    </div>
               </div>
            </section>
           )
}
export default GeneradorCards

