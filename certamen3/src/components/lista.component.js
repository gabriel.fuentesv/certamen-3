import { useEffect, useState } from 'react'
import axios from 'axios'
import GeneradorCards from '../components/generadorCards.component'
function Lista(props) {
  /*
  Props:
    token: str
  */

  /* Hooks */
  const [productos, setProductos] = useState([])
  const [pLoaded, setpLoaded] = useState(false)

  /* Funcionalidades */
  async function allProducts(token){
    console.log("Solicitando productos");
    let url = 'http://api.telematica.xkivertech.cl:4000/products';
    let options = {
        method: 'GET',
        headers: {
            'access-token': token
        },
        url
    };
    const respAuth = await axios(options);
    if(respAuth.data && productos.length === 0){
        var prodList = new Array()
        console.log("Guardando Productos")
        respAuth.data.forEach(a =>{
          prodList.push(a)
        })
        setProductos(prodList)
    }
  }
  useEffect(()=>{
    if(productos.length === 0){
      allProducts(props.token)
    } 
  }); 
    return(
        <div className="container">
            <GeneradorCards productos={productos}/>
            
        </div>
    )
}

export default Lista


/*
<Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src="holder.js/100px180" />
  <Card.Body>
    <Card.Title>Card Title</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Button variant="primary">Go somewhere</Button>
  </Card.Body>
</Card>
*/