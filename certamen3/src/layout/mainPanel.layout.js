import SpinnerLoader from '../components/spinner.component'
import Lista from '../components/lista.component'
import {Button} from 'react-bootstrap'
import { useEffect, useState } from 'react'
import axios from 'axios'
import qs from 'qs'
import { BsSearch } from 'react-icons/bs'
import Search from '../components/search.component'
function MainPanel (){
    /* Hooks */
    const [token, setToken] = useState("")
    const [loaded, setLoadedState] = useState(false)
    /* Funcionalidades */
    async function solicitarToken (){
        if (!loaded && !token) {
            console.log("Comenzando autenticacion");
            let url = 'http://api.telematica.xkivertech.cl:4000/auth/get-token';
            let data = {
                        k: '743c58fe-e323-452d-bb00-3bd8823e29ac',
                        s: 'Fd4LnTfHKk-m@!ge'
                        }
            let options = {
                method: 'POST',
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                },
                data: qs.stringify(data),
                url
            }
            const respAuth = await axios(options);
            if (respAuth.data.token) { 
                setToken(respAuth.data.token)
                setLoadedState(true)
                console.log("Autenticación completada con exito.")
            }
        }
    }

    useEffect(()=>{
        solicitarToken();
    })
    
    return (
        <div>
            <SpinnerLoader dataLoaded={loaded}/>
            <Lista token={token}/>
            <Button>
                <BsSearch/>
            </Button>

            <Search token={token} />
        </div>
    )
}

export default MainPanel