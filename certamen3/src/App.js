import MainPanel from './layout/mainPanel.layout'
import './App.css';

function App() {
  return (
    <div className="App">
      <MainPanel />
    </div>
  );
}

export default App;
